//go:build windows
// +build windows

package wgctrl

import (
	"gitee.com/aurawing/wgctl-go/internal/wginternal"
	"gitee.com/aurawing/wgctl-go/internal/wguser"
	"gitee.com/aurawing/wgctl-go/internal/wgwindows"
)

// newClients configures wginternal.Clients for Windows systems.
func newClients() ([]wginternal.Client, error) {
	var clients []wginternal.Client

	// Windows has an in-kernel WireGuard implementation.
	kc := wgwindows.New()
	clients = append(clients, kc)

	uc, err := wguser.New()
	if err != nil {
		return nil, err
	}

	clients = append(clients, uc)
	return clients, nil
}
