module gitee.com/aurawing/wgctl-go

go 1.20

require (
	gitee.com/aurawing/surguard-go v0.0.0-20230627084432-c354dbcc868c
	github.com/ethereum/go-ethereum v1.9.9
	github.com/google/go-cmp v0.5.9
	github.com/mdlayher/genetlink v1.3.2
	github.com/mdlayher/netlink v1.7.2
	github.com/mikioh/ipaddr v0.0.0-20190404000644-d465c8ab6721
	github.com/mr-tron/base58 v1.1.2
	golang.org/x/crypto v0.8.0
	golang.org/x/sys v0.9.0
)

require (
	github.com/btcsuite/btcd v0.0.0-20171128150713-2e60448ffcc6 // indirect
	github.com/josharian/native v1.1.0 // indirect
	github.com/mdlayher/socket v0.4.1 // indirect
	golang.org/x/net v0.9.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
)
